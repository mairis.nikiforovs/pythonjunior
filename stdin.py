import sys
from main import P

# Reads how many arguments have been passed from the command line
arguments = len(sys.argv) - 1

# Try/Catch block to handle invalid input (int is expected)
try:
    # Handles scenario with no parameters and prints a message
    if arguments == 0:
        print("Please enter the arguments!")
        print("Format: days_after_peak  days_in_a_row (Optional)")
    # Calls the function with default value if only 1 parameter entered(days_after_peak)
    elif arguments == 1:
        print(P(int(sys.argv[1])))
    # Calls the function with both parameters
    else:
        print(P(int(sys.argv[1]),int(sys.argv[2])))
# Displays an error message if input is not Integer
except ValueError:
    print("[Error] The input should be an integer!")