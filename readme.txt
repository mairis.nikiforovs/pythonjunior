This project consists of 3 python files:
    main.py
    stdin.py
    test/main.test.py

main.py contains the main recursive function that calculates the probability of contracting COVID in any given day
(takes 2 int variables as inputs) and returns it as a float value

stdin.py is a script that takes command line arguments, calls the recursive function and prints out the result.
The input format is:    python3 stdin.py int int(optional)
If 1 argument is entered, days_in_a_row is set to 1 as a default


test/main.test.py contains a Pytest class that has 4 unit tests.
To run the tests you need to either run the script with a pytest run configurator or
execute the following command in the terminal, given you are in this projects directory:
python -m pytest