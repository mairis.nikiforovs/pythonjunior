# Function that Calculates the probability of getting COVID,
# dependant on days after peak and days in a row(default = 1)
def P(days_after_peak=1, days_in_a_row=1):
    # Logic to handle weekend detection
    if days_after_peak%7 == 6 or days_after_peak%7 == 5:
        probability = 100/(2 + days_after_peak**(0.5))*1.1
    else:
        probability = 100/(2 + days_after_peak**(0.5))
    # Loops through days in a row and recursively increases the probability
    for number_of_day_in_a_row in range(1,days_in_a_row):
        probability += P(days_after_peak - number_of_day_in_a_row, days_in_a_row -
                         number_of_day_in_a_row)**(1.5 - number_of_day_in_a_row)
    return probability



