from main import P

# Class that contains all Pytests
class TestClass:
    # Checks the Control Scenario
    def test_ControlExample(self):
        assert P(30, 3) == 17.796559644518428

    # Extra checks implemented in methods below
    def test_1(self):
        assert P(0, 1) == 50.0

    def test_2(self):
        assert P(1, 1) == 33.333333333333336

    def test_3(self):
        assert P(4, 2) == 30.176380902050415
